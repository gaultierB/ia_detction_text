#! /usr/bin/python
import cv2
import pytesseract
from imutils.object_detection import non_max_suppression
from matplotlib import pyplot as plt
import numpy as np

net = cv2.dnn.readNet('east_text_detection.pb')
layerNames = [
	"feature_fusion/Conv_7/Sigmoid",
	"feature_fusion/concat_3"]



pytesseract.pytesseract.tesseract_cmd = 'tesseract.exe'
camera = cv2.VideoCapture(0)

def captch_ex(frame):
    ret, mask = cv2.threshold(frame, 180, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(frame, frame, mask=mask)
    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    dilated = cv2.dilate(new_img, kernel, iterations=9)

    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    boxes = non_max_suppression(np.array(rects), probs=confidences)

    for contour in contours:
        [x, y, w, h] = cv2.boundingRect(contour)

        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 255), 2)

        cropped = frame[y:y + h, x:x + w]

        str = pytesseract.image_to_string(cropped)
        #print(str)
    return frame

def predictions(prob_score, geo):
    (numR, numC) = prob_score.shape[2:4]
    boxes = []
    confidence_val = []

    # loop over rows
    for y in range(0, numR):
        scoresData = prob_score[0, 0, y]
        x0 = geo[0, 0, y]
        x1 = geo[0, 1, y]
        x2 = geo[0, 2, y]
        x3 = geo[0, 3, y]
        anglesData = geo[0, 4, y]

        # loop over the number of columns
        for i in range(0, numC):


            (offX, offY) = (i * 4.0, y * 4.0)

            # extracting the rotation angle for the prediction and computing the sine and cosine
            angle = anglesData[i]
            cos = np.cos(angle)
            sin = np.sin(angle)

            # using the geo volume to get the dimensions of the bounding box
            h = x0[i] + x2[i]
            w = x1[i] + x3[i]

            # compute start and end for the text pred bbox
            endX = int(offX + (cos * x1[i]) + (sin * x2[i]))
            endY = int(offY - (sin * x1[i]) + (cos * x2[i]))
            startX = int(endX - w)
            startY = int(endY - h)

            boxes.append((startX, startY, endX, endY))
            confidence_val.append(scoresData[i])

    # return bounding boxes and associated confidence_val
    return (boxes, confidence_val)

def traitement_frame(frame):
    (H, W) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(frame, 1.0, (W, H),(123.68, 116.78, 103.94), swapRB=True, crop=False)
    return blob

while camera.isOpened():
    success, frame = camera.read()
    #np.resize(frame, (320 ,320))
    frame = cv2.resize(frame, (320,320))
    print(frame.shape)
    (frameW, frameH) = frame.shape[:2]
    #rW = frameW / 320
    #rH = frameH / 320

    blob = traitement_frame(frame)
    net.setInput(blob)
    (scores, geometry) = net.forward(layerNames)
    (boxes, confidence_val) = predictions(scores, geometry)
    boxes = non_max_suppression(np.array(boxes), probs=confidence_val)
    results = []

    # loop over the bounding boxes to find the coordinate of bounding boxes
    for (startX, startY, endX, endY) in boxes:
# scale the coordinates based on the respective ratios in order to reflect bounding box on the frameinal image
        #startX = int(startX * rW)
        #startY = int(startY * rH)
        #endX = int(endX * rW)
        #endY = int(endY * rH)

#extract the region of interest
        r = frame[startY:endY, startX:endX]
#configuration setting to convert image to string.
        configuration = ("-l eng --oem 1 --psm 8")
##This will recognize the text from the image of bounding box
        try:
            text = pytesseract.image_to_string(r, config=configuration)
        except:
            print('c casse')
# append bbox coordinate and associated text to the list of results
        results.append(((startX, startY, endX, endY), text))

    for ((start_X, start_Y, end_X, end_Y), text) in results:
    # display the text detected by Tesseract

    # Displaying text
        text = "".join([x if ord(x) < 128 else "" for x in text]).strip()
        if text:
            cv2.rectangle(frame, (start_X, start_Y), (end_X, end_Y),
            (0, 0, 255), 2)
            cv2.putText(frame, text, (start_X, start_Y - 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,0, 255), 2)

    cv2.imshow('Ma cam', frame)

    if cv2.waitKey(1) & 0XFF == ord('q'):
        break
